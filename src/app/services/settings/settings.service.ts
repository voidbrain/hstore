import { Injectable } from '@angular/core';

@Injectable({
  	providedIn: 'root'
})
export class SettingsService {

	constructor() { }

	public appName:string = "Hstore";
  	public serverAddress:string = "https://www.hultimate.net/ajax/moduli/api/";
    public purposes:string[] = ["client"];
    public purpose:number = 0;
    public datatables = ["scans"];
}
