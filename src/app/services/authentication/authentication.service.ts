import { Platform }        from '@ionic/angular';
import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SettingsService } from '../settings/settings.service';
import { ApiService } from '../../services/api/api.service';
 
const TOKEN_KEY = 'auth-token';
 
@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
 
    authenticationState = new BehaviorSubject(null);

    constructor(
        private plt: Platform,
        private appSettings: SettingsService,
        private api: ApiService,
    ) { 
        this.plt.ready().then(() => {
            this.checkToken();
        });
    }

    checkToken() {
        let res = localStorage.getItem(this.appSettings.appName+'_'+TOKEN_KEY);
        if (res) {
            this.authenticationState.next(true);
        }
    }

    login(loginForm) {
        loginForm.app_name = this.appSettings.appName;
        let params = { loginForm : JSON.stringify(loginForm) }
        this.api.get(this.appSettings.serverAddress + 'user', params).then(result=>{
            if(result.auth==true){
                localStorage.setItem(this.appSettings.appName+'_'+TOKEN_KEY, result.token);
                localStorage.setItem(this.appSettings.appName+'_user', loginForm.username);
                localStorage.setItem(this.appSettings.appName+'_password', loginForm.password);
                return this.authenticationState.next(true);
            }else{
                localStorage.removeItem(this.appSettings.appName+'_'+TOKEN_KEY);
                localStorage.removeItem(this.appSettings.appName+'_user');
                localStorage.removeItem(this.appSettings.appName+'_password');
                return this.authenticationState.next(false);
            }
        })
    }

    logout() {
        localStorage.removeItem(this.appSettings.appName+'_'+TOKEN_KEY);
        localStorage.removeItem(this.appSettings.appName+'_user');
        localStorage.removeItem(this.appSettings.appName+'_password');
        return this.authenticationState.next(null);
    }

    isAuthenticated() {
        return this.authenticationState.value;
    }
 
}