import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { NetworkService } from '../../services/network/network.service';
import { SettingsService } from '../settings/settings.service';
import { ToastService } from '../../services/toast/toast.service';

@Injectable({
	providedIn: 'root'
})

export class ApiService {
	
	url: string = '';

	constructor(
		public http: HttpClient,
		public networkService: NetworkService,
        private appSettings: SettingsService,
        private toastService: ToastService,
	) {}

	init(){
        this.url = this.appSettings.serverAddress +'/';
    }

  	async get(table: string, params?: any): Promise<any> {
        return new Promise((resolve) => {
            if(this.networkService.status._value){
                console.info('[API]: network available')
                this.http.get(this.url + table, { params: params }).toPromise()
                    .then((response) => {
        	      	    resolve(response);
    	    	    })
                    .catch((err) => {
                        // simple logging, but you can do a lot more, see below
                        console.error('An error occurred:', err);
                        let warnNegativeAmount = true;
                        this.toastService.forceMessage('Errore di rete. '+(warnNegativeAmount? '\nATTENZIONE: Prodotto non sincronizzato, riprova.' : ''));

                        this.toastService.presentToast(warnNegativeAmount);
                    });
             }else{
                console.warn('[API]: not available')
                //item.synced = 0;
                let response = { result : null };
                resolve(response);
            }
        });
    }

    async post(table: string, item: any, params?: any): Promise<any> {
        return new Promise((resolve) => {
            if(this.networkService.status){
                console.info('[API]: network available')
                params.item = item;
                this.http.post(this.url + table, params).subscribe((response) => {
                    resolve(response);
                });
            }else{
                console.warn('[API]: not available')
                item.synced = 0;
                let response = { item : item };
                resolve(response);
            }
        });
    }

    async delete(table: string, item: any): Promise<any> {
        return new Promise((resolve) => {
            if(this.networkService.status){
                console.info('[API]: network available')
                this.http.delete(this.url + table+'?id='+item.id).subscribe((response) => {
                    console.info('[API]: item deleted online: ',item)
                    resolve(item);
                });
            }else{
                console.warn('[API]: network not available')
                item.deleted = 1;
                item.synced = 0;
                console.info('[API]: item deleted offline: ',item)
                resolve(item);
            }
        });
    }
}
