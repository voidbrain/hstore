import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../../models/field.interface';
import { FieldConfig } from '../../models/field-config.interface';

@Component({
    selector: 'input-select',
    styleUrls: ['select.component.scss'],
    template: `
    <ion-item
        class="dynamic-field input-select"
        [formGroup]="group">
        <ion-label color="primary">{{ config.label }}</ion-label>
        <ion-select multiple="{{config.multiple}}" [formControlName]="config.name" (ionChange)=" triggerChange($event) ">
            <ion-select-option *ngFor="let option of config.options" [value]="option.id" [selected]="option.id == id_company">
                {{ option.name }}
            </ion-select-option>
        </ion-select>
    </ion-item>
    `
})
export class SelectComponent implements Field {
    config: FieldConfig;
    group: FormGroup;

    public triggerChange(event){
        //console.log(event,this.config)
    }
}
