import { Component, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../../models/field.interface';
import { FieldConfig } from '../../models/field-config.interface';

@Component({
    selector: 'date',
    styleUrls: ['date.component.scss'],
    template: `
        <ion-item
            class="dynamic-field date"
            [formGroup]="group">
            <ion-label color="primary">{{ config.label }}</ion-label>
            <ion-datetime display-format="DD-MM-YY">
                [formControlName]="config.name"
            ></ion-datetime>
        </ion-item>
    `
})

export class DateComponent implements Field {
    config: FieldConfig;
    group: FormGroup;
}
