import { Component, Input, ViewChild, ElementRef, Renderer } from '@angular/core';

@Component({
    selector: 'expandable',
    templateUrl: 'expandable.html',
    styleUrls: ['expandable.scss'],
})
export class ExpandableComponent {

	@ViewChild('expandWrapper', {read: ElementRef}) expandWrapper;
	@Input('expanded') expanded;
	

	expandHeight: number = 100;

	constructor(public renderer: Renderer) {

	}

	ngAfterViewInit(){
		this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + 'px'); 
        //this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', 'auto');
	}

}
