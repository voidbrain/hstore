import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/authentication/auth-guard.service';

const routes: Routes = [
	{ path: '', redirectTo: 'scan', pathMatch: 'full' },
    
    {    
        path: 'scan', 
        canActivate: [AuthGuardService],
        loadChildren: './pages/scans/detail/detail.module#DetailPageModule' },
    {    
        path: 'list', 
        canActivate: [AuthGuardService],
        loadChildren: './pages/scans/master/master.module#MasterPageModule' },
    { 
        path: 'login', loadChildren: './pages/user/public/login/login.module#LoginPageModule' 
    },
    { 
        path: 'logout', loadChildren: './pages/user/public/logout/logout.module#LogoutPageModule' 
    },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}