import { AuthenticationService } from './../../../../services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
 
@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
    loginForm;
    showError;
    
    constructor(
        private authService: AuthenticationService,
    ) { 
        this.loginForm = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }
 
    ngOnInit() {
        this.authService.authenticationState.subscribe(state => {
            this.showError = !(state || state == null);
        });
      }

    login() {
        let login = this.authService.login(this.loginForm.value);
    }
 
}