import { AuthenticationService } from './../../../../services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router }                from '@angular/router';
 
@Component({
    selector: 'app-logout',
    templateUrl: './logout.page.html',
    styleUrls: ['./logout.page.scss'],
})

export class LogoutPage {
 
    constructor(
        private authService: AuthenticationService,
        private router: Router,
    ) { }
 
    ionViewWillEnter() {
        this.logout();
    }

    logout() {
        let action = this.authService.logout();
    }
 
}