import { Component, ChangeDetectorRef } from '@angular/core';
import {  MenuController } from '@ionic/angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
import { SettingsService } from '../../../services/settings/settings.service';
import { ApiService } from '../../../services/api/api.service';
import { DbService } from '../../../services/db/db.service';
import { ToastService } from '../../../services/toast/toast.service';

@Component({
  selector: 'app-detail',
  templateUrl: 'detail.page.html',
  styleUrls: ['detail.page.scss'],
})
export class DetailPage {
    storeForm;
    productInfo;
    showOfflineMessage = false;

    constructor(
        public db: DbService,
        private qrScanner: QRScanner,
        private api: ApiService,
        private appSettings: SettingsService,
        private toastService: ToastService,
        private changeRef: ChangeDetectorRef,
        public menuCtrl: MenuController,
    ) { 
        this.storeForm = new FormGroup({
            productCode: new FormControl('',Validators.required),
            amount: new FormControl('', Validators.required),
            action: new FormControl('', Validators.required),
            username: new FormControl('', Validators.required),
            token: new FormControl('', Validators.required),
            app_name: new FormControl('', Validators.required),
        });

        this.storeForm.patchValue({ amount: 1 });
    }

    ngOnInit(){
        this.db.load();

        let amount = parseInt(localStorage.getItem(this.appSettings.appName+'_defaultAmountValue'));
        amount = (isNaN(amount)?1:amount);

        
        this.storeForm.patchValue({ app_name: this.appSettings.appName });
        this.storeForm.patchValue({ username: localStorage.getItem(this.appSettings.appName+'_user') });
        this.storeForm.patchValue({ token: localStorage.getItem(this.appSettings.appName+'_auth-token') });
        this.storeForm.patchValue({ amount:amount });

        this.api.networkService.status.subscribe((networkStatus) => {
            console.info('[SCAN]: Network status: '+(networkStatus?'Online':'Offline'));                
            this.showOfflineMessage = !networkStatus;
        })

    }

    identifyProduct(code){
        this.storeForm.patchValue({ productCode: code });
        this.storeForm.patchValue({ action: 'check' });
        
        let params = { storeForm : JSON.stringify(this.storeForm.value) }
        this.api.get(this.appSettings.serverAddress + 'store', params)
            .then(response=>{
                
                if(response.result){
                    this.productInfo = JSON.parse(response.result);
                }else{
                    this.productInfo = {
                        productCode: code
                    };
                    // this.db.putItem('scans',{
                    //     productCode: code,
                    //     synced: false,
                    //     amount: this.storeForm.get('amount').value,
                    //     username: localStorage.getItem(this.appSettings.appName+'_user'),
                    //     date: Date.now() 
                    // })
                }
                this.changeRef.detectChanges();
            })
            .catch((e: any) => console.error('Network error is', e));
    }

    closeScanner(){
        this.productInfo = {};
        this.qrScanner.hide(); // hide camera preview
        this.hideCamera();
        this.qrScanner.destroy();
        this.menuCtrl.enable(true);
    }

    subValue(){
        let value = parseInt(this.storeForm.get('amount').value)-1;
        this.storeForm.patchValue({ amount: value });
        console.log(this.storeForm.get('amount').value)
    }
    addValue(){
        let value = parseInt(this.storeForm.get('amount').value)+1;
        this.storeForm.patchValue({ amount: value });
        console.log(this.storeForm.get('amount').value)
    }

    scanCode() {

        //this.identifyProduct('x11BDprovaM'); // prodotto trovato
        //this.identifyProduct('xxxxxxxxxxx'); // prodotto non trovato

        this.menuCtrl.enable(false);

        console.info('[SCAN] Scan start');
        this.qrScanner.getStatus().then((status) => {
            console.info(status)
        });

        this.qrScanner.prepare()
            .then((status: QRScannerStatus) => {
                console.info('[SCAN ]Scan status', status);
                if (status.authorized) {
                    // camera permission was granted
                    // start scanning
                    let scanSub = this.qrScanner.scan().subscribe((text: any) => {
                        let code = text.split('=').pop();
                        scanSub.unsubscribe(); // stop scanning
                        this.closeScanner();
                        this.identifyProduct(code);
                        
                    });
                    this.showCamera();
                    this.qrScanner.resumePreview();
                    // show camera preview
                    this.qrScanner.show()
                        .then((data: QRScannerStatus) => {
                            console.info('datashowing', data.showing);
                        }, err => {
                            console.error('show error', err);
                        });
                } else if (status.denied) {
                    // camera permission was permanently denied
                    // you must use QRScanner.openSettings() method to guide the user to the settings page
                    // then they can grant the permission from there
                } else {
                    // permission was denied, but not permanently. You can ask for permission again at a later time.
                }
            })
            .catch((e: any) => console.error('Scan error is', e));
    }

    showCamera() {
        window.document.querySelector('html').classList.add('cameraView');
        // window.document.querySelector('html').style.backgroundColor = 'transparent';
        // window.document.querySelector('body').style.backgroundColor = 'transparent';
        window.document.querySelector('ion-button.closeScanner').classList.remove('ion-hidden');
    }

    hideCamera() {
        window.document.querySelector('html').classList.remove('cameraView');
        // window.document.querySelector('html').style.backgroundColor = '';
        // window.document.querySelector('body').style.backgroundColor = '';
        let content = <HTMLElement>document.getElementsByTagName("body")[0];
        content.style.background = "black !important";
        window.document.querySelector('ion-button.closeScanner').classList.add('ion-hidden');
    }

    sendProductUpdate(){
        localStorage.setItem(this.appSettings.appName+'_defaultAmountValue',this.storeForm.get('amount').value);
        this.storeForm.patchValue({ action: 'update' });
        let params = { storeForm : JSON.stringify(this.storeForm.value) }
        this.api.get(this.appSettings.serverAddress + 'store', params).then(response=>{
            if(response.result){
                //this.identifyProduct(this.storeForm.get('productCode').value)
                let warnNegativeAmount = false;
                if(JSON.parse(response.result).disponibilita<0){
                    warnNegativeAmount = true;
                }

                this.toastService.forceMessage('Prodotto aggiornato. '+(warnNegativeAmount? '\nATTENZIONE: disponibilità minore di zero.' : ''));

                this.toastService.presentToast(warnNegativeAmount);
            }else{
                this.toastService.forceMessage('Prodotto salvato');
                this.toastService.presentToast();
                this.db.putItem('scans',{
                    productCode: this.storeForm.get('productCode').value,
                    synced: false,
                    amount: this.storeForm.get('amount').value,
                    username: localStorage.getItem(this.appSettings.appName+'_user'),
                    date: Date.now() 
                })
            }
            this.productInfo = {};
        }).catch((e: any) => console.error('Network error is', e));

        
    }
}



